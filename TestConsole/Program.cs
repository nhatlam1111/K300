﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestConsole
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("App " + args[0]);
            Thread.Sleep(10000);

            Process.GetCurrentProcess().Kill();
        }
    }
}
