﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Utility
{
    public static class ThreadController
    {
        private delegate void SetTextBox(Form f, Control ctrl, string text);

        private delegate void SetGridViewItem(Form f, DataGridView ctrl, string[] values, string tag);


        private delegate void ClearGridViewItem(Form f, DataGridView ctrl, string tag);

        private delegate void SetFormEnableDelegate(Form f, bool isEnable);

        private delegate void SetCloseFormDelegate(Form f);

        public static void SetText(Form form, Control ctrl, string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (ctrl.InvokeRequired)
            {
                SetTextBox d = new SetTextBox(SetText);
                form.Invoke(d, new object[] { form, ctrl, text });
            }
            else
            {
                ctrl.Text = text;
            }
        }

        public static void SetGridView(Form form, DataGridView ctrl, string[] values, string tag)
        {
            if (ctrl.InvokeRequired)
            {
                SetGridViewItem d = new SetGridViewItem(SetGridView);
                form.Invoke(d, new object[] { form, ctrl, values, tag });
            }
            else
            {
                bool isExists = false;

                foreach (DataGridViewRow dr in ctrl.Rows)
                {
                    if (dr.Tag + "" == tag)
                    {
                        for (int i = 0; i < dr.Cells.Count; i++)
                        {
                            try { dr.Cells[i].Value = values[i]; } catch { }
                        }

                        isExists = true;
                        break;
                    }
                }
                if (!isExists)
                {
                    ctrl.Rows.Insert(0, values);
                    ctrl.Rows[0].Tag = tag;

                    try
                    {
                        if (ctrl.Rows.Count > 5000)
                        {
                            ctrl.Rows.RemoveAt(ctrl.Rows.Count - 1);
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        }

      
        public static void ClearGridView(Form form, DataGridView ctrl, string tag)
        {
            if (ctrl.InvokeRequired)
            {
                ClearGridViewItem d = new ClearGridViewItem(ClearGridView);
                form.Invoke(d, new object[] { form, ctrl, tag });
            }
            else
            {
                if (string.IsNullOrEmpty(tag))
                {
                    ctrl.Rows.Clear();
                }
                else
                {
                    for (int i = 0; i < ctrl.Rows.Count; i++)
                    {
                        DataGridViewRow dr = ctrl.Rows[i];
                        if (dr.Tag + "" == tag)
                        {
                            ctrl.Rows.Remove(dr);
                            break;
                        }
                    }

                }
            }
        }
        public static void SetFormEnable(Form f, bool isEnable)
        {
            if (f.InvokeRequired)
            {
                SetFormEnableDelegate d = new SetFormEnableDelegate(SetFormEnable);
                f.Invoke(d, new object[] { f, isEnable });
            }
            else
            {
                f.Enabled = isEnable;
            }
        }

        public static void SetCloseForm(Form f)
        {
            if (f.InvokeRequired)
            {
                SetCloseFormDelegate d = new SetCloseFormDelegate(SetCloseForm);
                f.Invoke(d, new object[] { f });
            }
            else
            {
                f.Close();
                f.Dispose();
            }
        }
    }
}
