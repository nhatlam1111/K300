﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Utility
{
    public static class Database
    {
        private static OracleConnection _con;
        private static string connectionString = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST={0})(PORT={1}))(CONNECT_DATA=(SERVER=dedicated)(SERVICE_NAME={2})));User ID={3};Password={4};Pooling=true;Min Pool Size=1;Connection Lifetime=180;Max Pool Size=50;Incr Pool Size=5";
        public static string configFile = Directory.GetCurrentDirectory() + "\\db.config";

        public static int databasePort { get; set; }
        public static string databaseIP { get; set; }
        public static string username { get; set; }
        public static string password { get; set; }
        public static string databaseService { get; set; }
        public static string DEL_YN { get; set; }
        public static int reconnectTimeout = 30; //default 30 min
        public static DateTime lastconnect;
        public static DateTime reconnectStartTime { get; set; }
        public static DateTime reconnectEndTime { get; set; }

        public static ConnectionState State
        {
            get { return _con == null ? ConnectionState.Closed : _con.State; }
        }

        public static bool LoadConfig()
        {
            try
            {
                using (StreamReader sr = File.OpenText(configFile))
                {
                    string encryptStr = sr.ReadToEnd();
                    string decryptStr = EncryptionManagement.Decrypt(encryptStr, true);

                    List<string> infos = decryptStr.Split(';').ToList();
                    foreach (var info in infos)
                    {
                        string[] data = info.Split('|');

                        string value = data.Length == 2 ? data[1] : "";
                        switch (data[0])
                        {
                            case "serverip": databaseIP = value; break;
                            case "dbservice": databaseService = value; break;
                            case "dbuser": username = value; break;
                            case "dbpass": password = value; break;
                            case "serverport": databasePort = int.Parse(value); break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteLogError("config: " + e.Message);
                return false;
            }

            return true;
        }

        public static void SaveConfig()
        {
            try
            {
                string config = ToString();
                string encryptStr = EncryptionManagement.Encrypt(config, true);

                if (File.Exists(configFile))
                {
                    File.Delete(configFile);
                }

                using (FileStream fs = File.Create(configFile))
                {
                    Byte[] savebytes = new UTF8Encoding(true).GetBytes(encryptStr);
                    fs.Write(savebytes, 0, savebytes.Length);
                    fs.Close();
                    fs.Dispose();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }



        public static new string ToString()
        {
            return $"serverip|{databaseIP};serverport|{databasePort};dbuser|{username};dbpass|{password};dbservice|{databaseService}";
        }


        public static async Task<bool> Connect()
        {
            _con = new OracleConnection();
            _con.ConnectionString = string.Format(connectionString, databaseIP, databasePort, databaseService, username, password);

            try
            {
                await _con.OpenAsync();
                lastconnect = DateTime.Now;
                return true;
            }
            catch (Exception e)
            {
                WriteLogError("Connect Error: " + e.Message);
                Close();
                return false;
            }
        }

        public static void Close()
        {
            if (_con != null)
            {
                _con.Close();
                _con.Dispose();
                _con = null;
            }
        }

        public static async Task<bool> Reconnect()
        {
            bool isReconnected = false;
            Close();
            reconnectStartTime = DateTime.Now;

            do
            {
                if (await Connect())
                {
                    isReconnected = true; break;
                }
                else
                {
                    reconnectEndTime = DateTime.Now;
                }

                await Task.Delay(10000);

            } while ((reconnectEndTime - reconnectStartTime).TotalMinutes <= reconnectTimeout);

            if (!isReconnected)
            {
                WriteLogError(string.Format("Connect timeout ({0} min)  ---> Close application", reconnectTimeout));
            }

            return isReconnected;
        }

        public static void WriteLogError(string content)
        {
            string path = DateTime.Now.ToString("yyyyMMdd");
            string filePath = path + "\\LogErrorDB.txt";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }

            StreamWriter streamWriter = new StreamWriter(filePath, true, Encoding.Unicode);
            streamWriter.WriteLine(string.Format("{0}", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")));
            streamWriter.WriteLine(string.Format("{0}", content));
            streamWriter.WriteLine();
            streamWriter.Close();
        }

        public static async Task<DataTable> ExecuteProcedureCursor(string _proc, List<string> _params)
        {
            int idxPara = 0;
            DataTable dt = new DataTable();

            try
            {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = _con;
                cmd.CommandText = _proc;

                for (int i = 0; i < _params.Count; i++)
                {
                    cmd.Parameters.Add(":p_" + i, _params[i]);
                    idxPara = i;
                }

                //add return cursor
                cmd.Parameters.Add(":refcursor1", OracleDbType.RefCursor, ParameterDirection.Output);
                await cmd.ExecuteNonQueryAsync();
                OracleRefCursor curr = (OracleRefCursor)cmd.Parameters[idxPara + 1].Value;

                using (OracleDataReader dr = curr.GetDataReader())
                {
                    dt.Load(dr);
                }
            }
            catch (Exception e)
            {
                string err = "exec " + _proc + "('" + string.Join("','", _params) + "')";
                WriteLogError(err);
            }

            return dt;
        }

        public static DataTable ExecuteProcedureCursorNonAsync(string _proc, List<string> _params)
        {
            int idxPara = 0;
            DataTable dt = new DataTable();

            try
            {
                OracleCommand cmd = new OracleCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = _con;
                cmd.CommandText = _proc;

                for (int i = 0; i < _params.Count; i++)
                {
                    cmd.Parameters.Add(":p_" + i, _params[i]);
                    idxPara = i;
                }

                //add return cursor
                cmd.Parameters.Add(":refcursor1", OracleDbType.RefCursor, ParameterDirection.Output);
                cmd.ExecuteNonQuery();
                OracleRefCursor curr = (OracleRefCursor)cmd.Parameters[idxPara + 1].Value;

                using (OracleDataReader dr = curr.GetDataReader())
                {
                    dt.Load(dr);
                }
            }
            catch (Exception e)
            {
                string err = "exec " + _proc + "('" + string.Join("','", _params) + "')";
                WriteLogError(err);
            }

            return dt;
        }


    }
}
