﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace ConsoleMThreadsEat
{
    internal class Program
    {
        public static string ATTEN = "sp_sel_thr_machine_meal";
        public static string ATTEN_UPD = "SP_UPD_THR_TIME_TEMP_GENU_2_1";

        public static string machineID;

        private static async Task Main(string[] args)
        {
            if (args.Length == 0)
            {
                Process.GetCurrentProcess().Kill();
            }
            else
            {
                Database.databaseIP = args[0];
                Database.databasePort = int.Parse(args[1]);
                Database.databaseService = args[2];
                Database.username = args[3];
                Database.password = args[4];
                Database.DEL_YN = args[5];
                machineID = args[6];

                bool b = await Database.Reconnect();

                if (!b) //connect timeout
                {
                    Process.GetCurrentProcess().Kill();
                }

                await ProcessTime();
            }


        }

        private static async Task<bool> ProcessTime()
        {
            DataTable dtMachine = await Database.ExecuteProcedureCursor(ATTEN, new List<string> { machineID });
            if (dtMachine.Rows.Count <= 0)
            {
                return false;
            }


            List<Task> tasks = new List<Task>();

            foreach (DataRow dr in dtMachine.Rows)
            {
                var workThread = new WorkThread(int.Parse(dr["ID"].ToString()), dr["IP"].ToString(), 4370);
                tasks.Add(workThread.Process());
            }

            await Task.WhenAll(tasks);


            return true;

        }
    }
}
