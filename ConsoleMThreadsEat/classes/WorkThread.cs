﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utility;
using zkemkeeper;

namespace ConsoleMThreadsEat
{
    public enum ConnectionCode
    { 
        SDK_CONNECT,
        SDK_DISCONNECT,
        SDK_PROCESSED,
        DB_CLOSED
    }
    internal class WorkThread
    {
        private string sIP = "";
        private int iPort = 4370;
        private int iMachineNumber = 1;
        public CZKEMClass sdk;

        public int IMachineNumber;

        public WorkThread(int machine, string swIP, int iwPort)
        {
            this.IMachineNumber = machine;
            this.sIP = swIP;
            this.iPort = iwPort;
            sdk = new CZKEMClass();
        }

        public void WriteLog(string content, string id)
        {
            string path = "MEAL" + DateTime.Now.ToString("yyyyMMdd");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            StreamWriter streamWriter = new StreamWriter(path + "\\Log[" + id + "].txt", true, Encoding.Unicode);
            streamWriter.WriteLine(string.Format("{0} : {1}", (object)DateTime.Now.ToString("hh:mm:ss tt"), (object)content));
            streamWriter.Close();
        }

        public void WriteLogError(string content)
        {
            string path = "MEAL" + DateTime.Now.ToString("yyyyMMdd");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            StreamWriter streamWriter = new StreamWriter(path + "\\LogError.txt", true, Encoding.Unicode);
            streamWriter.WriteLine(string.Format("{0}", (object)DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")));
            streamWriter.WriteLine(string.Format("{0}", (object)content));
            streamWriter.WriteLine();
            streamWriter.Close();
        }

        public async Task<bool> Process()
        {
            await Task.Delay(1000);
            int iProcessTimeLimit = 10;
            int iProcessTime = 0;
            ConnectionCode processCode = ConnectionCode.SDK_DISCONNECT;

            while (++iProcessTime <= iProcessTimeLimit)
            {
                processCode = await WakeUpAsync();

                if (processCode == ConnectionCode.SDK_PROCESSED) break;

                if (processCode == ConnectionCode.DB_CLOSED)
                {
                    Console.WriteLine("Reconnect db " + this.sIP);
                    await Database.Reconnect(); //connect lai db
                }

                if (processCode == ConnectionCode.SDK_DISCONNECT)
                {
                    Console.WriteLine(iProcessTime+ ": Reconnect terminal " + this.sIP);
                    await Task.Delay(10000); // cho 10s connect lai terminal
                }
            }



            return true;
        }

        public async Task<ConnectionCode> WakeUpAsync()
        {
            
            if (!this.sdk.Connect_Net(this.sIP, this.iPort))
            {
                Console.WriteLine("*********Connecting " + this.sIP + " Failed......Current Time:" + DateTime.Now.ToLongTimeString());
                this.WriteLogError("Terminal " + this.IMachineNumber.ToString() + ": Connecting Failed " + DateTime.Now.ToLongTimeString());
                return ConnectionCode.SDK_DISCONNECT;
            }
            else
            {
                
                Console.WriteLine("*********Successfully Connect " + this.sIP);
                int num1 = 0;
                int num2 = 0;
                this.sdk.EnableDevice(this.IMachineNumber, true);

                if (Database.State == ConnectionState.Closed) return ConnectionCode.DB_CLOSED;

                this.sdk.EnableDevice(this.iMachineNumber, false);
                int num3 = 0;
                if (!this.sdk.GetDeviceStatus(this.IMachineNumber, 6, ref num3))
                { 
                    this.WriteLogError("Get record in terminal !!!");
                }


                if (this.sdk.ReadAllGLogData(this.IMachineNumber))
                {
                    int num4 = 0;
                    int num5 = 0;
                    int num6 = 0;
                    int num7 = 0;
                    int num8 = 0;
                    int num9 = 0;
                    int num10 = 0;
                    int num11 = 0;
                    int num12 = 0;
                    int num13 = 0;
                    int num14 = 0;
                    string str1 = "";
                    string str2 = "";
                    string str3 = "";
                    bool flag = false;
                    DataTable dataTable2 = new DataTable();
                    while (this.sdk.GetGeneralLogData(this.IMachineNumber, ref num4, ref num5, ref num6, ref num7, ref num8, ref num9, ref num10, ref num11, ref num12, ref num13))
                    {
                        if (this.sdk.GetUserInfo(this.IMachineNumber, num5, ref str2, ref str3, ref num14, ref flag))
                        { 
                            this.sdk.GetStrCardNumber(out str1);
                        }

                        List<string> strArray1 = new List<string> { "", "", "", "", "", "", "", ""};
                        List<string> strArray2 = strArray1;
                        int imachineNumber = this.IMachineNumber;
                        string str4 = imachineNumber.ToString();
                        strArray2[0] = str4;
                        strArray1[1] = num5.ToString();
                        strArray1[2] = "";
                        strArray1[3] = "";
                        strArray1[4] = str1 != "0" ? str1.ToString() : str3.ToString();
                        strArray1[5] = num9.ToString() + num10.ToString().PadLeft(2, '0').ToString() + num11.ToString().PadLeft(2, '0').ToString();
                        strArray1[6] = num12.ToString().PadLeft(2, '0').ToString() + ":" + num13.ToString().PadLeft(2, '0').ToString();
                        strArray1[7] = "genuwineat";
                        string content = "Call " + Program.ATTEN_UPD + "('" + string.Join("','", strArray1) + "')";

                        imachineNumber = this.IMachineNumber;

                        string id = imachineNumber.ToString();

                        this.WriteLog(content, id);

                        DataTable dtUpdResult = await Database.ExecuteProcedureCursor(Program.ATTEN_UPD, strArray1);
                        if (dtUpdResult.Rows.Count > 0 && dtUpdResult.Rows[0][0].ToString() == "1")
                        { 
                            ++num1;
                        }

                        Console.WriteLine("Inserted " + num1.ToString() + " rows");
                    }
                }
                else
                {
                    this.sdk.GetLastError(ref num2);
                    this.WriteLogError("Terminal " + this.IMachineNumber.ToString() + ": Unable to collect data from terminal");
                }
                this.WriteLog("==>Terminal ID [" + this.IMachineNumber.ToString() + " ] Total : " + (object)num1 + "/" + (object)num3 + " row(s).", this.IMachineNumber.ToString());
                if (Database.DEL_YN == "Y" && num3 == num1)
                {
                    this.WriteLog("Clear data terminal...", this.IMachineNumber.ToString());
                    this.sdk.ClearGLog(1);
                }
                this.sdk.EnableDevice(this.IMachineNumber, true);
                this.sdk.Disconnect();                
            }

            return ConnectionCode.SDK_PROCESSED;
        }

    }
}
