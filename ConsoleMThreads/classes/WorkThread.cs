﻿using ConsoleMThreads.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utility;
using zkemkeeper;

namespace ConsoleMThreadsEat
{
    public enum ConnectionCode
    { 
        SDK_CONNECT,
        SDK_DISCONNECT,
        SDK_PROCESSED,
        DB_CLOSED
    }
    internal class WorkThread
    {
        private string sIP = "";
        private int iPort = 4370;
        private int iMachineNumber = 1;
        public CZKEMClass sdk;

        public int IMachineNumber;

        public WorkThread(int machine, string swIP, int iwPort)
        {
            this.IMachineNumber = machine;
            this.sIP = swIP;
            this.iPort = iwPort;
            sdk = new CZKEMClass();
        }

        public void WriteLog(string content, string id)
        {
            string path = DateTime.Now.ToString("yyyyMMdd");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            StreamWriter streamWriter = new StreamWriter(path + "\\Log[" + id + "].txt", true, Encoding.Unicode);
            streamWriter.WriteLine(string.Format("{0} : {1}", (object)DateTime.Now.ToString("hh:mm:ss tt"), (object)content));
            streamWriter.Close();
        }

        public void WriteLogError(string content)
        {
            string path = DateTime.Now.ToString("yyyyMMdd");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            StreamWriter streamWriter = new StreamWriter(path + "\\LogError.txt", true, Encoding.Unicode);
            streamWriter.WriteLine(string.Format("{0}", (object)DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")));
            streamWriter.WriteLine(string.Format("{0}", (object)content));
            streamWriter.WriteLine();
            streamWriter.Close();
        }

        public async Task<bool> Process()
        {
            int iProcessTimeLimit = 10;
            int iProcessTime = 0;
            ConnectionCode processCode = ConnectionCode.SDK_DISCONNECT;

            while (++iProcessTime <= iProcessTimeLimit)
            {
                processCode = await WakeUpAsync();

                if (processCode == ConnectionCode.SDK_PROCESSED) break;

                if (processCode == ConnectionCode.DB_CLOSED)
                {
                    Console.WriteLine("Reconnect db " + this.sIP);
                    await Database.Reconnect(); //connect lai db
                }

                if (processCode == ConnectionCode.SDK_DISCONNECT)
                {
                    Console.WriteLine(iProcessTime + ": Reconnect terminal " + this.sIP);
                    await Task.Delay(10000); // cho 10s connect lai terminal
                }
            }



            return true;
        }

        public async Task<ConnectionCode> WakeUpAsync()
        {
            
            if (!this.sdk.Connect_Net(this.sIP, this.iPort))
            {
                Console.WriteLine("*********Connecting " + this.sIP + " Failed......Current Time:" + DateTime.Now.ToLongTimeString());
                this.WriteLogError("Terminal " + this.IMachineNumber.ToString() + ": Connecting Failed " + DateTime.Now.ToLongTimeString());
                return ConnectionCode.SDK_DISCONNECT;
            }
            else
            {
                Console.WriteLine("*********Successfully Connect " + this.sIP);
                int num = 0;
                int dwErrorCode = 0;
                this.sdk.EnableDevice(this.IMachineNumber, true);

                if (Database.State == ConnectionState.Closed) return ConnectionCode.DB_CLOSED;

                this.sdk.EnableDevice(this.iMachineNumber, false);

                int dwValue = 0;
                if (!this.sdk.GetDeviceStatus(this.IMachineNumber, 6, ref dwValue))
                {
                    this.WriteLogError("Get record in terminal !!!");
                }
                    
                if (this.sdk.ReadAllGLogData(this.IMachineNumber))
                {
                    int dwTMachineNumber = 0;
                    int dwEnrollNumber = 0;
                    int dwEMachineNumber = 0;
                    int dwVerifyMode = 0;
                    int dwInOutMode = 0;
                    int dwYear = 0;
                    int dwMonth = 0;
                    int dwDay = 0;
                    int dwHour = 0;
                    int dwMinute = 0;
                    int Privilege = 0;
                    string ACardNumber = "";
                    string Name = "";
                    string Password = "";
                    bool Enabled = false;
                    DataTable dataTable2 = new DataTable();
                    while (this.sdk.GetGeneralLogData(this.IMachineNumber, ref dwTMachineNumber, ref dwEnrollNumber, ref dwEMachineNumber, ref dwVerifyMode, ref dwInOutMode, ref dwYear, ref dwMonth, ref dwDay, ref dwHour, ref dwMinute))
                    {
                        if (this.sdk.GetUserInfo(this.IMachineNumber, dwEnrollNumber, ref Name, ref Password, ref Privilege, ref Enabled))
                        {
                            this.sdk.GetStrCardNumber(out ACardNumber);
                        }

                        List<string> strArray1 = new List<string> { "", "", "", "", "", "", "", "" };
                        List<string> strArray2 = strArray1;
                        int imachineNumber = this.IMachineNumber;
                        string str = imachineNumber.ToString();
                        strArray2[0] = str;
                        strArray1[1] = dwEnrollNumber.ToString();
                        strArray1[2] = "";
                        strArray1[3] = "";
                        strArray1[4] = ACardNumber != "0" ? ACardNumber.ToString() : Password.ToString();
                        strArray1[5] = dwYear.ToString() + dwMonth.ToString().PadLeft(2, '0').ToString() + dwDay.ToString().PadLeft(2, '0').ToString();
                        strArray1[6] = dwHour.ToString().PadLeft(2, '0').ToString() + ":" + dwMinute.ToString().PadLeft(2, '0').ToString();
                        strArray1[7] = "genuwin";
                        string content = "Call " + Program.ATTEN_UPD + "('" + string.Join("','", strArray1) + "')";
                        imachineNumber = this.IMachineNumber;
                        string id = imachineNumber.ToString();
                        this.WriteLog(content, id);
                        DataTable dtUpdResult = await Database.ExecuteProcedureCursor(Program.ATTEN_UPD, strArray1);
                        if (dtUpdResult.Rows.Count > 0 && dtUpdResult.Rows[0][0].ToString() == "1")
                            ++num;
                        Console.WriteLine("Inserted " + num.ToString() + " rows");
                    }
                }
                else
                {
                    this.sdk.GetLastError(ref dwErrorCode);
                    this.WriteLogError("Terminal " + this.IMachineNumber.ToString() + ": Unable to collect data from terminal");
                }
                this.WriteLog("==>Terminal ID [" + this.IMachineNumber.ToString() + " ] Total : " + (object)num + "/" + (object)dwValue + " row(s).", this.IMachineNumber.ToString());
                if (Database.DEL_YN == "Y" && dwValue == num)
                {
                    this.WriteLog("Clear data terminal...", this.IMachineNumber.ToString());
                    this.sdk.ClearGLog(1);
                }
                this.sdk.EnableDevice(this.IMachineNumber, true);
                this.sdk.Disconnect();
            }

            return ConnectionCode.SDK_PROCESSED;
        }

    }
}
