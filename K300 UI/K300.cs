﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utility;

namespace K300_UI
{
    public partial class Main : Form
    {

        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;

        private static string ProcessType;
        private static string DEL_YN = "N";


        public static string ATTEN = "SP_SEL_THR_MACHINE_INFO";
        public static string ATTEN_UPD = "SP_UPD_THR_TIME_TEMP_GENU_V2";

        public static string MEAL = "SP_SEL_THR_MACHINE_INFO_2";
        public static string MEAL_UPD = "SP_UPD_THR_TIME_TEMP_GENU_2_1";


        private List<Process> processes = new List<Process>();
        DataTable dtAttMachines;
        DataTable dtMealMachines;

        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);


        public Main(string[] args)
        {
            ProcessType = args[0];
            DEL_YN = args[1];

            //WriteLogError($"args: {args[0]} - {args[1]} ");


            InitializeComponent();

            progressBar.Visible = false;

            //tạm ẩn groupbox control
            groupBox3.Visible = false;

            this.Shown += new System.EventHandler(this.MainShown);

            Task.Run(async () =>
            {
                await StartApplication();
            });

            
        }

        private void MainShown(object sender, EventArgs e)
        {
            RenderApplication();
        }

        public void WriteLogError(string content)
        {
            string path = DateTime.Now.ToString("yyyyMMdd");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            StreamWriter streamWriter = new StreamWriter(path + "\\LogError.txt", true, Encoding.Unicode);
            streamWriter.WriteLine(string.Format("{0}", (object)DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")));
            streamWriter.WriteLine(string.Format("{0}", (object)content));
            streamWriter.WriteLine();
            streamWriter.Close();
        }

        private async Task StartApplication()
        {
            await InitDatabase();
        }

        private async void EndApplication()
        {
            while (processes.Where(q => !q.HasExited).Count() > 0 )
            {
                foreach (Process process in processes)
                {
                    process.Refresh();
                    if (process.HasExited)
                    {
                        Console.WriteLine($"Process {process.Id}: Closed");
                    }
                    else
                    {
                        Console.WriteLine($"Process {process.Id}: Running");
                    }
                    
                }
                await Task.Delay(1000);
            }

            this.Invoke((MethodInvoker) delegate
            {
                this.Close();
            });
        }

        public async Task<bool> InitDatabase()
        {
            await Task.Delay(1000);
            bool haveConfig = Database.LoadConfig();

            if (haveConfig)
            {
                ThreadController.SetText(this, txtIP, Database.databaseIP);
                ThreadController.SetText(this, txtPort, Database.databasePort+"");
                ThreadController.SetText(this, txtDbService, Database.databaseService);
                ThreadController.SetText(this, txtUser, Database.username);
                ThreadController.SetText(this, txtPass, Database.password);

                //???????????????????????????
                LoadDatabaseFromForm();

                await Database.Connect(); 

                
                return Database.State != ConnectionState.Closed;
            }
            else
            {
                MessageBox.Show("No config file for database!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public void RenderApplication()
        {
            int timeWaitConnect = 0;
            string fileName = "";
            Random rnd = new Random();

            while (Database.State == ConnectionState.Closed && timeWaitConnect++ < 10) 
            {
                Database.Connect().Wait();
                Thread.Sleep(1000);
            }

            if (Database.State == ConnectionState.Closed) return;

            DataTable dt;

            dtAttMachines = Database.ExecuteProcedureCursorNonAsync(ATTEN, new List<string> { "" });
            dtMealMachines = Database.ExecuteProcedureCursorNonAsync(MEAL, new List<string> { "" });



            if (ProcessType == "MEAL")
            {
                dt = dtMealMachines;
                fileName = "ConsoleMThreadsEAT.exe";
            }
            else
            {
                dt = dtAttMachines;
                fileName = "ConsoleMThreads.exe";
            }

            //WriteLogError($"ProcessType {ProcessType}, fileName {fileName}, machines {dt.Rows.Count} ");

            foreach (DataRow row in dt.Rows)
            {
                Process p = new Process();

                string[] args = {  
                    Database.databaseIP,
                    Database.databasePort+"",
                    Database.databaseService,
                    Database.username,
                    Database.password,
                    DEL_YN,
                    row["ID"]+""
                };

                p.StartInfo.FileName = fileName;
                p.StartInfo.Arguments = string.Join(" ", args);
                p.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;


                //WriteLogError($"Open {fileName} {p.StartInfo.Arguments}");


                p.Start();

                Thread.Sleep(3000);

                try
                {
                    SetParent(p.MainWindowHandle, panelSDK.Handle);

                    MoveWindow(p.MainWindowHandle, rnd.Next(0, 10)*50, rnd.Next(0, 10)*25, 400, 250, true);
                }
                catch (Exception e)
                {

                }

                processes.Add(p);
            }

        
            EndApplication();

        }

        private void LoadDatabaseFromForm()
        {
            Database.databaseIP = txtIP.Text;
            Database.databasePort = int.Parse(txtPort.Text);
            Database.databaseService = txtDbService.Text;
            Database.username = txtUser.Text;
            Database.password = txtPass.Text;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            LoadDatabaseFromForm();
            Database.SaveConfig();

            MessageBox.Show("Saved config!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnTestConnect_Click(object sender, EventArgs e)
        {
            LoadDatabaseFromForm();

            Task.Run(async () => {
                if (await Database.Connect())
                {
                    MessageBox.Show("Connected!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Cannot Connect!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            });
        }

        private void btnGetLogAtt_Click(object sender, EventArgs e)
        {

        }

        private void btnClearLogAtt_Click(object sender, EventArgs e)
        {

        }

        private void btnGetLogMeal_Click(object sender, EventArgs e)
        {

        }

        private void btnClearLogMeal_Click(object sender, EventArgs e)
        {

        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Database.Close();

            foreach (Process process in processes)
            {
                try
                {
                    process.Kill();
                }
                catch { }
                
            }
        }
    }
}
