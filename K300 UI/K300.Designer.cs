﻿namespace K300_UI
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelSDK = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgvTerminalList = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MachineName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnClearLogMeal = new System.Windows.Forms.Button();
            this.btnGetLogMeal = new System.Windows.Forms.Button();
            this.btnClearLogAtt = new System.Windows.Forms.Button();
            this.btnGetLogAtt = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnTestConnect = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDbService = new System.Windows.Forms.TextBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminalList)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSDK
            // 
            this.panelSDK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelSDK.Location = new System.Drawing.Point(12, 12);
            this.panelSDK.Name = "panelSDK";
            this.panelSDK.Size = new System.Drawing.Size(616, 437);
            this.panelSDK.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(634, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(338, 444);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgvTerminalList);
            this.groupBox3.Controls.Add(this.progressBar);
            this.groupBox3.Controls.Add(this.btnClearLogMeal);
            this.groupBox3.Controls.Add(this.btnGetLogMeal);
            this.groupBox3.Controls.Add(this.btnClearLogAtt);
            this.groupBox3.Controls.Add(this.btnGetLogAtt);
            this.groupBox3.Location = new System.Drawing.Point(6, 130);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(326, 308);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Control";
            // 
            // dgvTerminalList
            // 
            this.dgvTerminalList.AllowUserToAddRows = false;
            this.dgvTerminalList.AllowUserToDeleteRows = false;
            this.dgvTerminalList.AllowUserToResizeRows = false;
            this.dgvTerminalList.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvTerminalList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTerminalList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.MachineName,
            this.IP,
            this.Status});
            this.dgvTerminalList.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dgvTerminalList.Location = new System.Drawing.Point(6, 77);
            this.dgvTerminalList.Name = "dgvTerminalList";
            this.dgvTerminalList.RowHeadersVisible = false;
            this.dgvTerminalList.Size = new System.Drawing.Size(314, 196);
            this.dgvTerminalList.TabIndex = 18;
            // 
            // ID
            // 
            this.ID.Frozen = true;
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Width = 30;
            // 
            // MachineName
            // 
            this.MachineName.Frozen = true;
            this.MachineName.HeaderText = "Name";
            this.MachineName.Name = "MachineName";
            this.MachineName.ReadOnly = true;
            // 
            // IP
            // 
            this.IP.Frozen = true;
            this.IP.HeaderText = "IP";
            this.IP.Name = "IP";
            this.IP.ReadOnly = true;
            this.IP.Width = 80;
            // 
            // Status
            // 
            this.Status.Frozen = true;
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Width = 80;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(9, 279);
            this.progressBar.MarqueeAnimationSpeed = 20;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(309, 23);
            this.progressBar.Step = 1;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 17;
            // 
            // btnClearLogMeal
            // 
            this.btnClearLogMeal.Location = new System.Drawing.Point(164, 48);
            this.btnClearLogMeal.Name = "btnClearLogMeal";
            this.btnClearLogMeal.Size = new System.Drawing.Size(157, 23);
            this.btnClearLogMeal.TabIndex = 16;
            this.btnClearLogMeal.Text = "Delete Meal";
            this.btnClearLogMeal.UseVisualStyleBackColor = true;
            this.btnClearLogMeal.Click += new System.EventHandler(this.btnClearLogMeal_Click);
            // 
            // btnGetLogMeal
            // 
            this.btnGetLogMeal.Location = new System.Drawing.Point(5, 48);
            this.btnGetLogMeal.Name = "btnGetLogMeal";
            this.btnGetLogMeal.Size = new System.Drawing.Size(157, 23);
            this.btnGetLogMeal.TabIndex = 15;
            this.btnGetLogMeal.Text = "Get Meal";
            this.btnGetLogMeal.UseVisualStyleBackColor = true;
            this.btnGetLogMeal.Click += new System.EventHandler(this.btnGetLogMeal_Click);
            // 
            // btnClearLogAtt
            // 
            this.btnClearLogAtt.Location = new System.Drawing.Point(164, 19);
            this.btnClearLogAtt.Name = "btnClearLogAtt";
            this.btnClearLogAtt.Size = new System.Drawing.Size(157, 23);
            this.btnClearLogAtt.TabIndex = 14;
            this.btnClearLogAtt.Text = "Delete Attendance";
            this.btnClearLogAtt.UseVisualStyleBackColor = true;
            this.btnClearLogAtt.Click += new System.EventHandler(this.btnClearLogAtt_Click);
            // 
            // btnGetLogAtt
            // 
            this.btnGetLogAtt.Location = new System.Drawing.Point(5, 19);
            this.btnGetLogAtt.Name = "btnGetLogAtt";
            this.btnGetLogAtt.Size = new System.Drawing.Size(157, 23);
            this.btnGetLogAtt.TabIndex = 13;
            this.btnGetLogAtt.Text = "Get Attendance";
            this.btnGetLogAtt.UseVisualStyleBackColor = true;
            this.btnGetLogAtt.Click += new System.EventHandler(this.btnGetLogAtt_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnTestConnect);
            this.groupBox2.Controls.Add(this.btnSave);
            this.groupBox2.Controls.Add(this.txtPass);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtUser);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtDbService);
            this.groupBox2.Controls.Add(this.txtPort);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtIP);
            this.groupBox2.Location = new System.Drawing.Point(6, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(326, 123);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Database";
            // 
            // btnTestConnect
            // 
            this.btnTestConnect.Location = new System.Drawing.Point(74, 94);
            this.btnTestConnect.Name = "btnTestConnect";
            this.btnTestConnect.Size = new System.Drawing.Size(146, 23);
            this.btnTestConnect.TabIndex = 11;
            this.btnTestConnect.Text = "Test Connect";
            this.btnTestConnect.UseVisualStyleBackColor = true;
            this.btnTestConnect.Click += new System.EventHandler(this.btnTestConnect_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(226, 94);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(92, 23);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(226, 65);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(92, 20);
            this.txtPass.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "User - Pass";
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(74, 65);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(146, 20);
            this.txtUser.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Db Service";
            // 
            // txtDbService
            // 
            this.txtDbService.Location = new System.Drawing.Point(74, 39);
            this.txtDbService.Name = "txtDbService";
            this.txtDbService.Size = new System.Drawing.Size(244, 20);
            this.txtDbService.TabIndex = 4;
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(226, 13);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(92, 20);
            this.txtPort.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Host - Port";
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(73, 13);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(147, 20);
            this.txtIP.TabIndex = 2;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 461);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panelSDK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "K300";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminalList)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSDK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDbService;
        private System.Windows.Forms.Button btnTestConnect;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnClearLogMeal;
        private System.Windows.Forms.Button btnGetLogMeal;
        private System.Windows.Forms.Button btnClearLogAtt;
        private System.Windows.Forms.Button btnGetLogAtt;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.DataGridView dgvTerminalList;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MachineName;
        private System.Windows.Forms.DataGridViewTextBoxColumn IP;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}

